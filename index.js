const Benchmark = require('benchmark');
const AddressExisting = require('./existing/Address');
const AddressProposed = require('./proposed/Address');
const AddressNoDefineProperty = require('./proposed-no-defineproperty/Address');

const SMSExisting = require('./existing/SMS');
const SMSProposed = require('./proposed/SMS');
const SMSNoDefineProperty = require('./proposed-no-defineproperty/SMS');

const suite1 = new Benchmark.Suite('new Address');
console.log(suite1.name);
suite1.add('existing', function() {
	for (let i = 0; i < 1000; i++) {
		new AddressExisting();
	}
});
suite1.add('proposed', function() {
	for (let i = 0; i < 1000; i++) {
		new AddressProposed();
	}
});
suite1.add('noDefProp', function() {
	for (let i = 0; i < 1000; i++) {
		new AddressNoDefineProperty();
	}
});
suite1.on('cycle', function(event) {
	console.log(String(event.target));
});
suite1.on('complete', function() {
	console.log('Fastest is: ' + this.filter('fastest').map('name'));
	console.log();
});
suite1.run();


const suite2 = new Benchmark.Suite('new Address(data)');
console.log(suite2.name);
const addressData = {
	values: {
		objectuid: 123,
		type: 'B',
		address1: '100 N Philips Ave',
		address2: '',
		city: 'Sioux Falls',
		state: 'SD',
		province: null,
		county: null,
		postalCode: '57103', // implicit column name is 'postalcode'
		latitude: 44,
		longitude: -96,
		llpoint: '',
	}
};
suite2.add('existing', function() {
	for (let i = 0; i < 1000; i++) {
		new AddressExisting(addressData);
	}
});
suite2.add('proposed', function() {
	for (let i = 0; i < 1000; i++) {
		new AddressProposed(addressData);
	}
});
suite2.add('noDefProp', function() {
	for (let i = 0; i < 1000; i++) {
		new AddressNoDefineProperty(addressData);
	}
});
suite2.on('cycle', function(event) {
	console.log(String(event.target));
});
suite2.on('complete', function() {
	console.log('Fastest is: ' + this.filter('fastest').map('name'));
	console.log();
});
suite2.run();


const suite3 = new Benchmark.Suite('new SMS()');
console.log(suite3.name);
suite3.add('existing', function() {
	for (let i = 0; i < 1000; i++) {
		new SMSExisting();
	}
});
suite3.add('proposed', function() {
	for (let i = 0; i < 1000; i++) {
		new SMSProposed();
	}
});
suite3.add('noDefProp', function() {
	for (let i = 0; i < 1000; i++) {
		new SMSNoDefineProperty();
	}
});
suite3.on('cycle', function(event) {
	console.log(String(event.target));
});
suite3.on('complete', function() {
	console.log('Fastest is: ' + this.filter('fastest').map('name'));
	console.log();
});
suite3.run();


const suite4 = new Benchmark.Suite('new SMS(data)');
console.log(suite4.name);
const smsData = {
	values: {
		uid: '1234',
		campaignuid:'7890',
		name:'Blergh',
		fromname:'Blobble',
		type:'SMS',
		password: 'XXXXX',
		from: 'XXXXX',
		mo: 'asdf',
		id: 'XXXXX',
		username: 'XXXXX',
	}
};
suite4.add('existing', function() {
	for (let i = 0; i < 1000; i++) {
		new SMSExisting(smsData);
	}
});
suite4.add('proposed', function() {
	for (let i = 0; i < 1000; i++) {
		new SMSProposed(smsData);
	}
});
suite4.add('noDefProp', function() {
	for (let i = 0; i < 1000; i++) {
		new SMSNoDefineProperty(smsData);
	}
});
suite4.on('cycle', function(event) {
	console.log(String(event.target));
});
suite4.on('complete', function() {
	console.log('Fastest is: ' + this.filter('fastest').map('name'));
	console.log();
});
suite4.run();
