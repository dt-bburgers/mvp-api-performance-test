'use strict';

const DTFC = require('./DTFC');
const Message = require('./Message');

class SMS extends Message {}

DTFC.defineDatabaseClass(SMS, {
	definition: {
		password: 'XXXXX',
		from: 'XXXXX',
		mo: '',
		id: 'XXXXX',
		username: 'XXXXX',
	},
});

module.exports = SMS;
