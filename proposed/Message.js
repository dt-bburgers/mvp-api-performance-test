'use strict';

const DTFC = require('./DTFC');

class Message extends DTFC {
}

DTFC.defineDatabaseClass(Message, {
	table: 'message',
	primaryKey: ['uid'],
	definition: {
		uid: '',
		campaignuid:'',
		name:'',
		fromname:'',
		type:'',
	},
});

module.exports = Message;
