'use strict';

const EventEmitter = require('events').EventEmitter;

const tableNameSymbol = Symbol('tableName');
const tableDefinitionSymbol = Symbol('tableDefinition');
const primaryKeySymbol = Symbol('primaryKey');

class DTFC {
	constructor(options = {}) {
		this._initializeValues(DTFC.getTableDefinition(this.constructor));
		if (options.values) {
			for (const k in options.values) {
				if (this.hasOwnProperty(k)) {
					this[k] = options.values[k];
				}
			}
		}
	}

	_initializeValues(tableDefinition) {
		for (const propertyName of Object.keys(tableDefinition)) {
			const columnDefinition = tableDefinition[propertyName];
			this[propertyName] = columnDefinition.defaultValue;
		}
	}

	/**
	 * Turn a class that inherits from DTFC into a database class
	 */
	static defineDatabaseClass(dataClass, { table, primaryKey, definition } = {}) {
		if (!table) {
			table = DTFC.getTableName(Object.getPrototypeOf(dataClass));
		}

		if (!primaryKey) {
			primaryKey = DTFC.getPrimaryKey(Object.getPrototypeOf(dataClass));
		}

		dataClass[tableNameSymbol] = table;
		dataClass[tableDefinitionSymbol] = DTFC._buildTableDefinition(dataClass, definition);
		dataClass[primaryKeySymbol] = primaryKey;
	}

	static _buildTableDefinition(dataClass, definition) {
		const parentClass = Object.getPrototypeOf(dataClass);
		const actualDefinition = Object.assign({}, DTFC.getTableDefinition(parentClass));

		if (!definition) { return actualDefinition; }

		for (const key of Object.keys(definition)) {
			const val = definition[key];

			const inferredColumnName = key.toLowerCase();

			if (typeof val == 'object') {
				actualDefinition[key] = {
					columnName: val.columnName || inferredColumnName,
					defaultValue: val.defaultValue || null,
				};
			} else {
				actualDefinition[key] = {
					columnName: inferredColumnName,
					defaultValue: val,
				};
			}
		}

		return actualDefinition;
	}

	/**
	 * Get the name of the table for a DTFC data class
	 *
	 * @return {String} The table name
	 */
	static getTableName(dataClass) {
		return dataClass[tableNameSymbol];
	}

	/**
	 * Get the column definitions for a DTFC data class
	 *
	 * @return {Object[]} An array of objects that represent a column definition
	 */
	static getTableDefinition(dataClass) {
		return dataClass[tableDefinitionSymbol];
	}

	/**
	 * Get the primary key for a DTFC data class
	 *
	 * @return {String[]} The property names that comprise the primary key
	 */
	static getPrimaryKey(dataClass) {
		return dataClass[primaryKeySymbol];
	}

	/**
	 * Get a class definition suitable to be passed into Object.defineProperties
	 */
	static getClassDefinition(dataClass) {
		const classDefinition = {};

		// If it's an event listener, we want to explicitely mark non-enumerable
		// some of it's event listener properties.
		if (dataClass.prototype instanceof EventEmitter) {
			classDefinition.domain = {
				writable: true,
				enumerable: false,
				configurable: true,
			};
			classDefinition._events = {
				writable: true,
				enumerable: false,
				configurable: true,
			};
			classDefinition._eventsCount = {
				writable: true,
				enumerable: false,
				configurable: true,
			};
			classDefinition._maxListeners = {
				writable: true,
				enumerable: false,
				configurable: true,
			};
		}

		// Then build out the property definitions based on the column
		// definitions. Because the table definition already deals with
		// inheritance, we don't need to traverse up the inheritance chain.
		const tableDefinition = DTFC.getTableDefinition(dataClass);
		for (const key of Object.keys(tableDefinition)) {
			classDefinition[key] = {
				configurable: true,
				enumerable: true,
				writable: true,
				value: tableDefinition[key].defaultValue,
			};
		}

		return classDefinition;
	}

	toJSON() {
		const ret = {};
		for (const prop in this) {
			if (this.propertyIsEnumerable(prop)) {
				ret[prop] = this[prop];
			}
		}

		return ret;
	}
}

module.exports = DTFC;
