'use strict';

// Basic example of creating a Database class.

// We need DTFC. It's both the base class and provides 'defineDatabaseClass'.
const DTFC = require('./DTFC');

// Extend from DTFC to get toJSON and whatever else we add to DTFC in the
// future.
class Address extends DTFC {
}

// Define the database properties of the class.
DTFC.defineDatabaseClass(Address, {
	// What table this class corresponds to.
	table: 'address',
	// What columns constitute the primary key.
	primaryKey: ['objectuid', 'type'],

	// Column definitions. This is either in the form of
	//
	//   propertyName: [defaultValue]
	//
	// in which case the column name is inferred
	// or, if the property name needs to differ from the column name,
	//
	//   propertyName: {
	//     columnName: [columnName]
	//     defaultValue: [defaultValue]
	//   }
	definition: {
		objectuid: '',
		type: '',
		address1: '',
		address2: '',
		city: '',
		state: '',
		province: '',
		county: '',
		postalCode: '', // implicit column name is 'postalcode'
		latitude: '',
		longitude: '',
		llpoint: '',
	},
});

module.exports = Address;
