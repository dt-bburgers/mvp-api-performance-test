'use strict';

const parent = require('./DTFC');
const _ = require('lodash');

const Address = function() {
	parent.apply(this, arguments);
};

const obj = {
	class_def() {
		return {
			objectuid: {
				configurable: true,
				enumerable: true,
				value: '',
				writable: true,
			},

			type: {
				configurable: true,
				enumerable: true,
				value: '',
				writable: true,
			},

			address1: {
				configurable: true,
				enumerable: true,
				value: '',
				writable: true,
			},

			address2: {
				configurable: true,
				enumerable: true,
				value: '',
				writable: true,
			},

			city: {
				configurable: true,
				enumerable: true,
				value: '',
				writable: true,
			},

			state: {
				configurable: true,
				enumerable: true,
				value: '',
				writable: true,
			},

			province: {
				configurable: true,
				enumerable: true,
				value: '',
				writable: true,
			},

			county: {
				configurable: true,
				enumerable: true,
				value: '',
				writable: true,
			},

			postalcode: {
				configurable: true,
				enumerable: true,
				value: '',
				writable: true,
			},

			latitude: {
				configurable: true,
				enumerable: true,
				value: '',
				writable: true,
			},

			longitude: {
				configurable: true,
				enumerable: true,
				value: '',
				writable: true,
			},

			llpoint: {
				configurable: true,
				enumerable: true,
				value: '',
				writable: true,
			},
		};
	},

	table() {
		return 'address';
	},

	primary_keys() {
		return ['objectuid', 'type'];
	},
};

Address.prototype = Object.create(parent.prototype);
Address.prototype.constructor = Address;
_.extend(Address.prototype, obj);
module.exports = Address;
