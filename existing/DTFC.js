'use strict';

const _ = require('lodash');
const events = require('events');

function getClassDef(obj) {
	const classdef = {};
	function class_def(obj) {
		if (obj !== null) {
			const o = Object.getPrototypeOf(obj);
			if (o !== null) {
				class_def(o);
			}
			if (obj.hasOwnProperty('class_def')) {
				_.extend(classdef, obj.class_def());
			}
		}
	}
	class_def(obj);

	return classdef;
}

const DTFC = function(options) {
	options = options || {};
	events.EventEmitter.call(this);
	Object.defineProperties(this, getClassDef(this));
	if (options.values) {
		for (const k in options.values) {
			if (this.hasOwnProperty(k)) {
				this[k] = options.values[k];
			}
		}
	}
};

const dtfc = {
	class_def() {
		// these hide properties that we get from EventEmitter
		return {
			domain: {
				writable: true,
				enumerable: false,
				configurable: true,
			},
			_events: {
				writable: true,
				enumerable: false,
				configurable: true,
			},
			_eventsCount: {
				writable: true,
				enumerable: false,
				configurable: true,
			},
			_maxListeners: {
				writable: true,
				enumerable: false,
				configurable: true,
			},
		};
	},

	table() {
		return '';
	},

	primary_keys() {
		return [''];
	},

	toJSON() {
		const self = this;
		const ret = {};
		let prop;
		for (prop in self) {
			if (prop === '_events') {
				continue; // return undefined;
			}
			if (prop === 'domain' && !self.hasOwnProperty(prop)) {
				continue; // return undefined;
			}
			if (self.propertyIsEnumerable(prop)) {
				ret[prop] = self[prop];
			}
		}

		return ret;
	},
};

DTFC.prototype = Object.create(events.EventEmitter.prototype);
DTFC.prototype.constructor = DTFC;
_.extend(DTFC.prototype, dtfc);
module.exports = DTFC;
