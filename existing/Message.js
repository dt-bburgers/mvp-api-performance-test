'use strict';

const parent = require('./DTFC');
const _ = require('lodash');

const Message = function() {
	parent.apply(this, arguments);
};

const obj = {
	class_def() {
		return {
			uid: {
				configurable: true,
				enumerable: true,
				value: '',
				writable: true,
			},

			campaignuid: {
				configurable: true,
				enumerable: true,
				value: '',
				writable: true,
			},

			name: {
				configurable: true,
				enumerable: true,
				value: '',
				writable: true,
			},

			fromname: {
				configurable: true,
				enumerable: true,
				value: '',
				writable: true,
			},

			type: {
				configurable: true,
				enumerable: true,
				value: '',
				writable: true,
			},
		};
	},

	table() {
		return 'message';
	},

	primary_keys() {
		return ['uid'];
	},
};

Message.prototype = Object.create(parent.prototype);
Message.prototype.constructor = Message;
_.extend(Message.prototype, obj);
module.exports = Message;
