'use strict';

const parent = require('./Message');
const _ = require('lodash');

const SMS = function() {
	parent.apply(this, arguments);
};

const obj = {
	class_def() {
		return {
			password: {
				configurable: true,
				enumerable: true,
				value: 'XXXXX',
				writable: true,
			},

			from: {
				configurable: true,
				enumerable: true,
				value: 'XXXXX',
				writable: true,
			},

			mo: {
				configurable: true,
				enumerable: true,
				value: '',
				writable: true,
			},

			id: {
				configurable: true,
				enumerable: true,
				value: 'XXXXX',
				writable: true,
			},

			username: {
				configurable: true,
				enumerable: true,
				value: 'XXXXX',
				writable: true,
			},
		};
	},

	table() {
		return 'message';
	},

	primary_keys() {
		return ['uid'];
	},
};

SMS.prototype = Object.create(parent.prototype);
SMS.prototype.constructor = SMS;
_.extend(SMS.prototype, obj);
module.exports = SMS;
