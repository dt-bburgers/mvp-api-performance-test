Jon asked me to do a performance test on the proposed API vs the existing API.

While looking at our throughput last week, I also noticed that we were spending
a lot of time in `Object.defineProperty`, and after some research found that
`Object.defineProperty` is typically [very slow][1]. So I also included a
version that doesn't use `Object.defineProperty` and instead sets the values
directly. This version *may* be functionally different.

```
new Address
existing x 35.13 ops/sec ±2.66% (46 runs sampled)
proposed x 50.70 ops/sec ±2.26% (56 runs sampled)
noDefProp x 431 ops/sec ±2.43% (77 runs sampled)
Fastest is: noDefProp

new Address(data)
existing x 33.34 ops/sec ±4.99% (46 runs sampled)
proposed x 53.43 ops/sec ±1.63% (57 runs sampled)
noDefProp x 355 ops/sec ±2.80% (72 runs sampled)
Fastest is: noDefProp

new SMS()
existing x 43.32 ops/sec ±1.92% (55 runs sampled)
proposed x 58.02 ops/sec ±2.54% (58 runs sampled)
noDefProp x 529 ops/sec ±1.93% (72 runs sampled)
Fastest is: noDefProp

new SMS(data)
existing x 40.04 ops/sec ±2.69% (51 runs sampled)
proposed x 58.78 ops/sec ±2.25% (59 runs sampled)
noDefProp x 408 ops/sec ±1.70% (78 runs sampled)
Fastest is: noDefProp
```

*Note: in this case, an "op" is instantiating the object 1,000 times. So we're
actually looking at about ~40,000 and ~400,000 instantiations/sec.*


## Result

It appears that the proposed and existing API styles are roughly equivalent, and
the proposed API style without using `Object.defineProperty` is up to an order
of a magnitude faster.

However, my **opinion** is that this is definitely a micro benchmark, and the
actual differences most likely won't affect the actual throughput of the API in
any significant way.

[1]: https://www.nczonline.net/blog/2015/11/performance-implication-object-defineproperty/